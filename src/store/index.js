import Vue from 'vue'
import Vuex from 'vuex'
import decode from 'jwt-decode'

import router from '../router'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    /* El state es de solo lectura */
    token: null,
    user:null 
  },
  mutations: {
    /* Logica para modificar el state */
    // El payload es el token
    setToken(state,token){
     state.token = token; 
    },
    setUser(state,user){
      state.user = user; 
    }
    
  },
  actions: {
     /* Acciones que son similares a las mutaciones */
    /* Las acciones realizar mutaciones */
    saveToken({commit},token){
      commit("setToken",token)
      commit('setUser',decode(token))
      localStorage.setItem('token',token)

    },
    
    autoLogin({commit}){
      /* ========== CODE ORIGINAL> ============ */
      // let token = localStorage.getItem('token')
      // if(token){
      //   commit('setToken',token)
      //   commit('setUser',decode(token))
      // }  
      //Redirect User to home
      // router.push({name:'Home'});  
      /* ========= </CODE ORIGINAL> ============ */
      let token = localStorage.getItem('token')
      if(token){
        commit('setToken',token)
        commit('setUser',decode(token))
        //Redirect User to home
        router.push({name:'Home'});  
      }
      /*=========REVISAR ESTA PARTE ============ */
    },
    /* logIn */
    logOut({commit}){
      commit('setToken',null);
      commit('setUser',null);
      localStorage.removeItem('token');
      router.push({name:'Login'})

    }
  },
  getters:{
    
  },
  modules: {
  }
})
