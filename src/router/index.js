import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '../store'
import Home from '../views/Home.vue'
import User from '../views/User.vue'
import Loan from '../views/Loan.vue'
import Equipment from '../views/Equipment.vue'
import Company from '../views/Company.vue'
import Manager from '../views/Manager.vue'
import Area from '../views/Area.vue'
import Login from '../views/Login.vue'
import Password from '../views/Password.vue'
import Reset from '../views/Reset.vue'
import MessageReset from '../views/MessageReset.vue'

// import Report from '../views/Report.vue'


Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta:{
      ADMIN:true,
    }
  },
  {
    path: '/about',
    name: 'About',
    component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  },
  {
    // path:'/usuarios',
    path:'/empleados',
    name:'User',
    component: User,
    meta:{
      ADMIN:true,
    }
  },
  {
    path:'/prestamo',
    name:'Loan',
    component: Loan,
    meta:{
      ADMIN:true,
    }
  },
  {
    path:'/equipos',
    name:'Equipment',
    component: Equipment,
    meta:{
      ADMIN:true,
    }
  },
  {
    path:'/empresa',
    name:'Company',
    component: Company,
    meta:{
      ADMIN:true,
    }
  },
  {
    path:'/encargado',
    name:'Manager',
    component: Manager,
    meta:{
      ADMIN:true,
    }
  },
  {
    path:'/area',
    name:'Area',
    component: Area,
    meta:{
      ADMIN:true,
    }
  },

  {
    path:'/login',
    name:'Login',
    component: Login,
    meta:{
      libre:true,
    }
  },
  {
    path:'/recover-password',
    name:'Password',
    component: Password,
    meta:{
      libre:true,
    }
  },
  {
    // path:'/password-reset/:userId/:token',
    // path:'/password-reset/:userId/:token',
    path:'/password-reset/:userId/:token', 
    name:'Reset',
    component: Reset,
    meta:{
      libre:true,
    }
  },
  {
    path:'/message-reset',
    name:'MessageReset',
    component: MessageReset,
    meta:{
      libre:true,
    }
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to,from,next)=>{
  if(to.matched.some(record => record.meta.libre)){
    next();
  }else if(store.state.user && store.state.user.role == "ADMIN"){
    if(to.matched.some(record => record.meta.ADMIN)){
      next();
    }  
  } else if(store.state.user && store.state.user.role == "USER"){
    if(to.matched.some(record => record.meta.USER)){
      next();
    }  
  }else{
    next({name:'Login'});
  } 
})
export default router
